abstract type AbstractTrigger end 

struct NoTrigger <: AbstractTrigger end
get_callback(::NoTrigger, ::Any) = nothing

#==============================  ImportTrigger  ===============================# 

struct ImportTrigger{T, T2} <: AbstractTrigger 
    times::Tuple{Float64, Float64}
    value::T2
    p::T
end
ImportTrigger() = ImportTrigger((2., 3.), 10., :r_i)
ImportTrigger(times, value) = ImportTrigger(times, value, :r_i)


function get_callback(trig::ImportTrigger, model)
    idx=findfirst(==(trig.p), params(model)) 
    return multiplicative_trigger(trig.times, trig.value, idx)
end

#==============================  ExportTrigger  ===============================# 

struct ExportTrigger{T, T2} <: AbstractTrigger 
    times::Tuple{Float64, Float64}
    value::T2
    p::T
end
ExportTrigger() = ExportTrigger((2., 3.), 10., :r_e)
ExportTrigger(times, value) = ExportTrigger(times, value, :r_e)


function get_callback(trig::ExportTrigger, model)
    idx=findfirst(==(trig.p), params(model)) 
    return multiplicative_trigger(trig.times, trig.value, idx)
end


#=============================  InfusionTrigger  ==============================# 

struct InfusionTrigger{T, T2} <: AbstractTrigger 
    time::Float64
    value::T2
    p::T
end
InfusionTrigger() = InfusionTrigger(1., 100., 1)

function get_callback(trig::InfusionTrigger, model)
    if trig.p isa Symbol
        idx=findfirst(==(trig.p), model.syms) 
    else
        idx = trig.p
    end

    return PresetTimeCallback([trig.time], i -> (i.u[idx] = trig.value))
end

#==================================  Utils  ===================================# 


function multiplicative_trigger(tspan, value, idx)
    apply_trigger!(i) = (i.p[idx] *= value)
    remove_trigger!(i) = (i.p[idx] /= value)
    return CallbackSet(
        PresetTimeCallback([tspan[1]], apply_trigger!),
        PresetTimeCallback([tspan[2]], remove_trigger!),
    )
end

