% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[
]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{booktabs}
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  hidelinks,
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering

\author{}
\date{}

\usepackage[]{graphicx}
\begin{document}


\section*{Mathematical modelling of MYB3R4 nuclear localisation}

Here, we use mathematical modelling to analyse the dynamics of a CYT-induced
nuclear localisation of MYB3R4 in order to better compare the idea with
experimental observations.

\subsection*{CYT-affected nuclear flux is consistent with data}
% Here, we analyse the dynamics of a CYT-induced nuclear localisation of
% MYB3R4 and the effects that MYB3R4 - IMPA3/6 feedback could have to
% those dynamics.

Since we do not know the mechanism through which CYT affects the nuclear flux of
MYB3R4, we first studied the dynamical difference between CYT increasing import
or decreasing export. To do this, we created a simple deterministic model, based
on the law of mass action, wherein MYB3R4 is shuttled in and out of the nucleus
(with respective import and export rate constants \(r_i\) and \(r_e\)).  We
separate separate nuclear-localised MYB3R4, $N$, and cytosolic MYB3R4, $C$, and
we assume that the total amount of MYB3R4, $N + C = R4_{tot}$, is constant.  We
further make the simple assumption that the rate of import or export is
directly proportional to the availability of cytosolic and nuclear MYB3R4.  From
these assumptions, we created a model where CYT increases the rate of import
(the ``import model''): 
%
\begin{equation} 
    \frac{d N (t)}{dt} = \underbrace{ r_{i} \cdot \overbrace{ \left( k +  CYT
    \right)}^{\textrm{CYT contrib.} } \cdot \overbrace{ \left( R4_{tot} -  N
    \right)}^{\textrm{Cytosolic MYB3R4}}}_\textrm{Nuclear import} -
    \underbrace{r_{e} \cdot N}_{\textrm{Nuclear export}} .
    \label{eq:import_model} 
\end{equation}

Here, CYT is a model input and \(k\) is a parameter that helps set the
constitutive import rate. This constitutive import mainly affects the
model dynamics at low CYT inputs.

We also created a model where CYT decreases the export rate (the
``export model''):
\begin{equation}
\frac{d N (t)}{dt} = r_{i} \cdot \left( R4_{tot} -  N  \right) - \frac{r_{e}}{k +  CYT } \cdot  N 
.
\label{eq:export_model}
\end{equation}

Here, \(k\) also determines the model behaviour by limiting the effect
of low CYT, but this time through ensuring a maximal export-rate in the
absence of CYT.

With identical parameter values, the steady states of these two models
are also identical. 
\begin{equation}
 N_{eq} =
\frac{r_{i} \cdot \left( k +  CYT  \right) \cdot R4_{tot}}{r_{i} \cdot \left( k +  CYT  \right)  + r_e}
\label{eq:n_eq}
\end{equation}

However, by comparing the derivatives for the two models, labelled
\(N_{i}\) and \(N_e\) for the import or the export model, respectively,
we get

\[
    \frac{d N_i}{dt} = \left(k + CYT \right) \cdot \frac{d N_e}{dt} 
\]

The two models share limit-behaviour but the dynamics of the export
model slows down in comparison to the import model when CYT is applied.

Equilibration is only reached asymptotically, meaning that the system
never actually gets there. Therefore, we measure the equilibration time,
\(t_{eq}\) as the time it takes to get half-way to the equilibrium from
one state.

For the import model, we have that

\begin{equation}
t_{eq} =  \frac{\log\left( 2 \right)}{r_{e} + r_i \cdot \left( k + cyt \right)}.
\label{eq:t_eq_imp}
\end{equation}

while the export model equilibrates according to

\begin{equation}
t_{eq} = \frac{\log\left( 2 \right)  }{\frac{r_{e}}{\left( k + cyt \right)} + r_i }
\label{eq:t_eq_exp}
\end{equation}

Thus, if CYT induced nuclear import then increasing the CYT level would
speed up equilibration while equilibration would be slowed down if CYT
repressed nuclear export.

For any given value of CYT, the export model can be as fast as the
import value if both the import and export rate parameters, \(r_i\) and
\(r_e\), are a factor \(\left( k + cyt \right)\) larger than that of the
import model. However, the two models will still behave differently if
cyt varies between or within simulations. Tuning parameters such that
the two models match for one cyt input will ensure that they do not
match for any other.



Three pieces of experimental data were important for the model specification and
parameter inference. The first one regards the equilibration time, $t_{eq}$, of
the MYB3R4 localisation. To estimate this, we used a previously measured
equilibration time for importin-mediated nuclear import in mamalian cells [cite
paper]. From that data, we assume that 
\begin{equation}
    t_{eq} = 6 \pm 3 \mathrm{min}.
\end{equation}
Here, and in the rest of the document $x \pm y$ should be read to mean an
estimated value of $x$ with an estimated uncertainty of $y$. 
The second measurement was the ratio of nuclear to cytosolic localisation of
MYB3R4 before the CYT peak. This was measured through quantification of
fluorescence intensity using ImageJ [Cite, weibing expand?]. Since the measured
data included cells in all stages of cell division we used k-means clustering to
separate cells with a high or low nuclear to cytoplasmic ratio. We
assume that, unlike the group with the low ratio, the group with the high ratio
has already been exposed to the CYT peak. From the mean and standard deviation
of the low ratio cells [Fig SI1a] we estimate that the pre-CYT nuclear to
cytosolic localisation of MYB3R4, $\Delta$, was 
\begin{equation}
    \Delta \equiv \frac{N_{eq}}{C_{eq}} = 0.81 \pm 0.23.
\end{equation}
The third measurement was the ratio between the peak concentration of CYT
to the basal CYT concentration [Cite relevant paper],
\begin{equation}
    \sigma \equiv \frac{CYT_{peak}}{CYT_{basal}} = \frac{CYT_{peak}}{k}  = 5 \pm 2.
    \label{eq:sigma}
\end{equation}

To mimic the CYT
peak prior to cell division, we used an impulse input, at $t=1$, with a
gamma-distributed delay to get [Cite korsbo]
%
\begin{equation}
    CYT\left( t \right) = 
    \begin{cases}
        0 & t < 1 \\    
        \frac{\gamma  \cdot r^n  \cdot (t-1)^{n-1} \cdot e^{-r \cdot (t - 1)}}{\left( n - 1 \right)!} &
        t \geq 1.
    \end{cases}
\end{equation}
%
The shape parameters, $n$ and $r$, were set to $6$ and $10$, respectively, to
loosely mimic the observed CYT peak shape. 
$\gamma$ is a scaling parameter for which the main dynamical dynamical effect
comes from its ratio to the $k$ parameter. This ratio is constrained by
equation~\ref{eq:sigma} since $\gamma$ controls the CYT peak height. With this
constraint, and with the way we calculate $r_i$ and $r_e$, the actual values of
$k$ and $\gamma$ does not matter much. For deterministic simulations, they only
serve to uniformly scale the solution. For stochastic simulations their scaling
effect determines the degree to with noise affects the CYT peak. The values
were set to $k = 100$ and $\gamma = 2500 \pm 1000$, which fulfills equation
equation~\ref{eq:sigma} and ensures a somewhat realistic signal to noise ratio
of stochastic simulations of the CYT peak (fig stochastic run). 

% The value of the scaling parameter was constrained in two ways. First by
% equation~\ref{eq:sigma} since $\gamma$ affects the CYT peak height, second by 

% The scaling parameter was set to
% $\gamma = 5  \cdot  \sigma$ which ensured a peak height 
% of about $\sigma$ and
% which constrained the models basal CYT level, $k$, to a value of $k = 1$. 






%All dynamically important model input and parameters where inferred from experimental observations. 
%% The model's parameters and input were set in accordance with
%% experimental observations. 
%$R4_{tot}$ and $k$ were not dynamically important and simply set to one since
%$R4_{tot}$ merely scales the solution and any non-zero value of $k$ can be
%compensated for by adjusting $r_i$, $r_e$ and the $CYT$ input.  To mimic the CYT
%peak prior to cell division, we used an impulse input, at $t=1$, with a
%gamma-distributed delay to get
%%
%\begin{equation}
%    CYT\left( t \right) = 
%    \begin{cases}
%        0 & t < 1 \\    
%        \frac{\gamma  \cdot r^n  \cdot (t-1)^{n-1} \cdot e^{-r \cdot (t - 1)}}{\left( n - 1 \right)!} &
%        t \geq 1.
%    \end{cases}
%\end{equation}
%%
%The shape parameters, $n$ and $r$, were set to $6$ and $10$, respectively, to
%loosely mimic the observed CYT peak shape. The scaling parameter was set to
%$\gamma = 25 \pm 10$ from an assumption that the peak CYT level is roughly five
%times that of normal CYT concentrations (cf. basal and peak CYT values in XXX).
%Here, $x \pm y$ should be read to mean an estimated value of $x$ with an
%estimated uncertainty of $y$ but it should be noted that, while informed by data,
%both these numbers are guesses.  
%% The remaining rate parameters were set through two experimental measurements.
%% The remaining rate parameters was 
%The remaining rate parameters were set by two assumptions. 
%First, that the equilibration time of nuclear import/export is $t_{eq} =
%6 \pm 3 \textrm{min}$ (based on a study in mammalian cells, XXX).
%Second, we assume that the ratio
%of nuclear to cytosolic MYB3R4 at equilibrium is $\Delta = 0.81 \pm 0.23$ (fig
%XXX). This was based on measurements of nuclear and cytosolic fluorescence
%intensity in MYB3R4::GFP. The ratio of nuclear to cytosolic fluorescence was
%calculated and  clustered using k-means clustering with two groups. The clustering
%identifies two groups of data with different nuclear to cysololic MYB3R4
%localisation (Fig \ref{fig:exp_vs_imp}a). We think that the group of high
%nuclear to cytosolic ratio are cells undergoing division whereas the cluster of
%low nuclear to cytosolic ratio reflect cells wherein the rapid nuclear flux of
%MYB3R4 has not yet been initiated. This second group of low nuclear to cytosolic
%ratio should then represent the nuclear to cytosolic localisation balance
%of MYB3R4 prior to the CYT peak. The mean and standard deviation of these ratios
%were measured to get the aforementioned values.

The import and export rate parameters were calculated based on $\Delta$ and
$t_{eq}$. We assumed that the measured $\Delta$ describes the nuclear to
cytosolic ratio at equilibrium, when there is no CYT peak. 
This enables us to combine equation~\ref{eq:n_eq} with
equation~\ref{eq:t_eq_imp} for the import model or equation~\ref{eq:t_eq_exp}
for the export model. For the import model, we then get
\begin{equation}
    r_{i} = \frac{\log\left( 2 \right)}{t_{eq} \cdot \left( k + CYT_{eq} \right)  \cdot \left( 1 + \frac{1}{\Delta} \right)}
= 0.031 \pm 0.016
\end{equation}
and
\begin{equation}
r_{e} = \frac{\log\left( 2 \right)}{t_{eq} \cdot \left( 1 + ratio \right)} = 3.8 \pm 2
\end{equation}
For the export model, we instead get
\begin{equation}
r_{i} = \frac{\log\left( 2 \right)}{t_{eq} \cdot \left( 1 + \frac{1}{\Delta}
\right)} = 3.1 \pm 1.6
\end{equation}
and 
\begin{equation}
    r_{e} = \frac{\log\left( 2 \right) \cdot \left( k + CYT_{eq} \right) }{t_{eq}
    \cdot \left( 1 + \Delta \right)} = 380 \pm 200.
\end{equation}
With this, all the parameters that are relevant for the dynamics of
deterministic model simulations have been anchored to experimental observations
and we are left with models with no degree of freedom at all. 


These experimentally-derived model conditions lead to good agreement between
model dynamics and experimental observations.
% Using these experimentally-derived model conditions, the export and the import
% model allow for good, but imperfect, model/data agreement.  
Upon exposure to a
peak of CYT, both models lead to a rapid and distinct nuclear localisation of
MYB3R4.  Our experiments show that the ratio of nuclear to cytosolic MYB3R4
increases from $0.81 \pm 0.23$ to $4.4 \pm 2.3$ and that this takes in the order of an hour from
the start of nuclear accumulation (Fig timecourse).  Both models capture these dynamics
fairly well, but where one is stronger, the other is weaker.  The import
model very accurately captures the response strength but its dynamics is a bit faster
than observed (Figure~\ref{fig:exp_vs_imp}). The export model, on the other
hand, captures the temporal dynamics better but it results in a response that is
weaker than observed (Figure~\ref{fig:exp_vs_imp}f).
Even so, considering that the model parameters were fully set through
experimental measurements, the resulting model dynamics capture the nuclear
localisation of MYB3R4 remarkably well.

% We would here like to emphasise that the dynamics of the models are constrained
% by the assumptions that we made when choosing input and parameter values. If
% tuned for a much faster nucleocytic equilibration both models can achieve
% faster and stronger responses to the CYT input. 

\begin{figure}[htpb]
    \centering
    \includegraphics[width=1\textwidth]{../results/exp_vs_imp_uncertainty.pdf}
    \caption{
        \textbf{CYT affecting import or export leads to different MYB3R4
        responses.}
        a) 
        Measurements of nuclear and cytosolic fluorescence intensity in
        MYB3R4::GFP. K-means clustering reveals two groups, one with a high
        nuclear to cytosolic ratio (I) and one with low (II).
        % Diagram representing computational models wherein MYB3R4 is shuttled back and
        % forth across the nulear membrane and where CYT either facilitates import
        % ('import model') or represses export ('export model'). 
        %
        b, d, f) Time-course dynamics of the import and export models when
        exposed to a CYT pulse (b). d) shows the levels of nuclear localised
        MYB3R4 while f) shows the ratio between nuclear localised and cytosolic
        MYB3R4. The target peak value is estimated from the mean and standard
        deviation of group I measurements in a) and is shown in grey.
        %
        c) The nuclear-cytosolic localisation equilibration time for the import
        and export model when exposed to different levels of CYT. 
        %
        e) The equilibrium values of nuclear MYB3R4 for different CYT
        levels.
        Solid lines show the results based on the central values of our
        assumptions while the ribbons show the propagated uncertainty from those
        assumptions. 
    }
    \label{fig:exp_vs_imp}
\end{figure}

\begin{table}[h]
    \centering
    \caption{Parameter values for the import and export models (no feedback).}
    \label{tab:param_exp_imp}
    \begin{tabular}{ccc}
        Parameter & Import model & Export model\\
        $r_{i}$ & $0.031 \pm 0.016$ & $3.1 \pm 1.6$\\
        $k$ & $100.0 $ & $100.0 $\\
        $r_{e}$ & $3.8 \pm 2.0$ & $380.0 \pm 200.0$\\
        $R4_{tot}$ & $1000.0 $ & $1000.0 $\\
    \end{tabular}
\end{table}

\subsection{The MYB3R4 - IMPA3/6 feedback could serve to increase the MYB3R4
nuclear localisation conferred by CYT}

Here, we probe the dynamical consequences of the observed MYB3R4-IMPA3/IMPA6
feedback.
% Here, we use mathematical modelling to explore some possible dynamical consequences of the observed
% MYB3R4-IMPA3/6 feedback.
This is done in a simplified way by extending the models above with a
nuclear import feedback term that increases in strength the more nuclear
MYB3R4 there is. 
For the import model, we then get
% \begin{equation}
%     \underbrace{
%     \frac{v  \cdot N}{\alpha + N}  
% }_{\text{Hill type activation}}
%     \cdot 
%     \overbrace{
%     \left( k + CYT \right)
% }^{\text{CYT induction}}
%     \cdot 
%     \underbrace{
%     \left( R_{tot} - N \right) 
% }_{\text{Cytosolic MYB3R4}}
% \end{equation}
\begin{align}
    \frac{dN(t)}{dt} =& \underbrace{\left( r_{i}  + \frac{v \cdot N}{\alpha
    + N}\right) }_{\textrm{Constitutive + feedback}} \cdot \left( R4_{tot} - N
\right) \cdot \left( k + CYT \right) - r_{e} \cdot N.
\label{eq:imp_fb}
\end{align}
For the export model, we instead get
\begin{align}
    \frac{dN(t)}{dt} =& \underbrace{\left( r_{i}  + \frac{v \cdot N}{\alpha
    + N}\right) }_{\textrm{Constitutive + feedback}} \cdot \left( R4_{tot} - N
\right) - \frac{r_{e} \cdot N}{\left( k + CYT \right) } .
\label{eq:exp_fb}
\end{align}
Here, the new parameter $\alpha$ determines the value of $N$ at which the
feedback has half its maximal effect. This is the only parameter value that we
lack some evidence for but its dynamical effect is rather limited. For $\alpha
<< R4_{tot}$, we get that the feedback is always on at its maximal value,
effectively turning the feedback term into a constant and removing the actual
feedback from the model. Rejecting this scenario, we set $\alpha = R4_{tot}$ ,
which ensures that the feedback term never comes close to saturation in the
simulation. Further increases of $\alpha$ can mostly be compensated for by a
matching increase in $v$, rendering the dynamics mostly unaffected.  The
parameter $v$ scales the contribution of the feedback term.  We think of the
feedback term to represent the action of IMAP3/IMPA6 while the constitutive
import term, $r_i$, represents the action of other importins.  From confocal
images, we estimate that IMPA3/IMPA6 abundance in the late cell cycle is about
five times higher than the total impotin abundance in the earlier stages of the
cell cycle [ref to figure?]. To emulate this, we set the $v$ parameter to ensure
ensure that the feedback term had a maximal contribution that was about five
times higher than the constitutive import rate, $r_i$
(Table~\ref{tab:param_feedback}). 

% to Equation~\ref{eq:import_model}.
% For the export model, where CYT does not affect the import, we instead add a term 
% \begin{equation}
%     \underbrace{
%     \frac{v  \cdot N}{\alpha + N}  
% }_{\text{Hill type activation}}
%     \cdot 
%     \underbrace{
%     \left( R_{tot} - N \right) 
% }_{\text{Cytosolic MYB3R4}}
% .
% \end{equation}

The import and export rate parameters, $r_i$ and $r_e$, were calculated from the
same measurements used in the non-feedback models. 
However, since the model equations are different, so are the expressions which
map these observations to the parameter values. 
To derive the new expressions, we first assumed that the value of
$t_{eq}$ was measured in the absence of a feedback $v=0$. This means that the
equations~\ref{eq:t_eq_imp} and \ref{eq:t_eq_exp} still hold for the feedback
import and export models, respectively.  This, combined with the model
equations~\ref{eq:imp_fb} and \ref{eq:exp_fb} can be solved for $r_i$ and $r_e$.
For the import model with feedback, we get
\begin{equation}
    r_i = \frac{\frac{\log\left( 2 \right)}{t_{eq} \cdot \left( k + cyt_{eq} \right)} - \frac{\beta}{\Delta}}{1 + \frac{1}{\Delta}}
\end{equation}
where 
\begin{equation}
\beta = \frac{v \cdot \left( \frac{\Delta \cdot R4_{tot}}{1 + \Delta} \right)}{\alpha + \left( \frac{\Delta \cdot R4_{tot}}{1 + \Delta} \right)}
\end{equation}
and
\begin{equation}
    r_e = \frac{\frac{\log\left( 2 \right)}{t_{eq}} + \beta \cdot \left( k +
    cyt_{eq} \right)}{1 + \Delta}.
\end{equation}
For the export model with feedback, we instead get 
\begin{equation}
r_i = \frac{\frac{\log\left( 2 \right)}{t_{eq}} - \frac{\beta}{\Delta}}{1 + \frac{1}{\Delta}}
\end{equation}
and 
\begin{equation}
r_e = \frac{\left( \frac{\log\left( 2 \right)}{t_{eq}} + \beta \right) \cdot
\left( \alpha + cyt_{eq}\right)}{1 + \Delta}.
\end{equation}
The values of $k$ and $R4_{tot}$ were the same as for the non-feedback models
and the final values of all parameters are listed in Table~\ref{tab:param_feedback}.

\begin{table}[h]
    \centering
    \caption{Parameter values for feedback models}
    \label{tab:param_feedback}
    \begin{tabular}{ccc}
        Parameter & Import model & Export model\\
        $v_{i}$ & $0.1 $ & $10.0 $\\
        $r_{i}$ & $0.014 \pm 0.016$ & $1.4 \pm 1.6$\\
        $\alpha$ & $1000.0 $ & $1000.0 $\\
        $k$ & $100.0 $ & $100.0 $\\
        $r_{e}$ & $5.5 \pm 2.0$ & $550.0 \pm 200.0$\\
        $R4_{tot}$ & $1000.0 $ & $1000.0 $\\
        % $n_{hill}$ & $1.0 $ & $1.0 $\\
    \end{tabular}
\end{table}

% experimental observations. For deterministic simulations, we could have set
% $R4_{tot}$, $k$,  and $\gamma$ like we did for the simpler models above. However,
% here, we wanted the ability to run stochastic simulations. Since these
% parameters act as scaling factors for $N$ and $CYT$ and such scaling affect the
% degree of stochasticity, we set them to $R4_{tot}=1000$, $k=100$ and
% $\gamma=2500$. 
% % matters for 
% $r_i$ and $r_e$ were set by assuming that the experimentally observed nuclear
% localisation equilibration occurred in absentia of the feedback ($v = 0$), but
% that the observed nuclear to cytosolic MYB3R4 ratio occurred at a time where the
% feedback was in effect (since MYB3R4 and IMPA3/6 are expressed close to cell
% division which is when the cytosolic to nuclear ratio was measured).
% This lead to  
% \begin{equation}
%     r_i = \frac{\frac{\log\left( 2 \right)}{t_{eq} \cdot \left( k + cyt_{eq} \right)} - \frac{\beta}{\Delta}}{1 + \frac{1}{\Delta}}
% \end{equation}
% where 
% \begin{equation}
% \beta = \frac{v \cdot \left( \frac{\Delta \cdot R4_{tot}}{1 + \Delta} \right)}{\alpha + \left( \frac{\Delta \cdot R4_{tot}}{1 + \Delta} \right)}
% \end{equation}
% and
% \begin{equation}
%     r_e = \frac{\frac{\log\left( 2 \right)}{t_{eq}} + \beta \cdot \left( k +
%     cyt_{eq} \right)}{1 + \Delta}.
% \end{equation}
% % $v$ was set to ensure a roughly three times higher that $r_i$. 
% $v$ was set to ensure that the import rate contribution of the feedback term was
% roughly three times higher than that of the constitutive import term when $N
% =R4_{tot}$. For the import model we used $v=0.1$ and for the export model, we
% used $v=10$. 
% These values were chosen
% though estimation of IMPA3/IMPA6 availability compared to the availability of
% non-cell cycle bound importins. The feedback term thus represents the added
% import through IMPA3/IMPA6 while the constitutive import term represents the action
% of other importins. 
% $n$ was set to $1$ since this assumes the simplest form of activation and since
% we have no direct evidence for what this value should be, we opted for the
% simplest assumption. 
% We had no experimental evidence with which to fix the value of $\alpha$, which
% we set to $\alpha = R4_{tot}$, ensuring that the activation term does not fully saturate
% even if all MYB3R4 is localised to the nucleus. 


\begin{figure}[htpb]
    \centering
    \includegraphics[width=\textwidth]{../results/feedback_bio_param_uncertainty.pdf}
    \caption{
        \textbf{The MYB3R4-IMPA3/6 feedback could ensure a
stronger nucleocytic trafficking response to CYT input.
}
a, c and e) Simulations of the export model with or without a positive feedback
on nuclear MYB3R3 ($N$) and further nuclear localisation. a) Model input in the
form of a CYT peak. c) The effect of the CYT peak on nuclear-localised MYB3R4.
e) The nuclear to cytosolic MYB3R4 ratio of the models compared to the observed
nuclear to cytosolic MYB3R4 ratio in group I of Figure SI-1a [xxx fix]. 
b, d and f) Repeats of a, c and e, but for the import model.
g and h) The change-rate of nuclear MYB3R4 depending on how much of the MYB3R4 is
already in the nucleus for the export and import models, respectively. Equilibria is found where the model lines cross 0 (grey
line). In the absence of CYT, the system equilibrates to $N \approx 500$. A
sudden increase to a CYT input of $400$ (c.f. peak in a, b) would jump the
system over to the dashed lines. 
Lines show results using the central value of our assumptions and ribbons show
the propagated uncertainty from those assumptions. 
}
    \label{fig:feedback}
\end{figure}


The addition of feedback to the models result in stronger and faster MYB3R4
nuclear localisation responses to CYT input. 
Except for when the concentration of nuclear MYB3R4 is very low, the addition of
the feedback results in faster MYB3R4 localisation responses for both the export
and the import models (Fig SI2g,h).
This ensures not only a faster but also a stronger MYB3R4 nuclear localisation
response to the CYT input (Fig SI2). 
Without feedback, the import model seemed to have a slightly better fit with
data.
However, with the feedback, its response to CYT is both a bit faster and a bit
stronger than experimental observations lead us to expect. 
The export model, on the other hand, benefits from the addition of feedback and
now reproduces the target data very well. 
However, the differences are still too small to use the model performances to
conclusively say how CYT affects the nuclear flux of MYB3R4. 
In reality, all models here described fit the data very well considering the
severe constraints put on their parameter values. We take this to strongly
support our hypothesis that the CYT pulse before cell division act to shift the
localisation of MYB3R4 from the cytosol to the nucleus where the MYB3R4 can
trigger downstream signalling. 


% \subsection{Putting our full hypothesis into a model}%
% \label{sub:putting_our_full_hypothesis_into_a_model}

% In this paper, we use and produce experimental evidence to develop an hypothesis
% about how CYT, MYB3R4 and IMPA3/6 interacts to control the localisation of
% MYB3R4 and thus, presumably, to control onset of cell division. 
% Here, we formalise the hypothesis in mathematical terms and test whether it is
% capable of capturing experimentally observed dynamics. 

% In this model, we 




% \subsection{Spontaneous rapid nuclear localisation is possible but inconsistent with data}%
% \label{sub:spontaneous_rapid_nuclear_localisation_is_possible_but_inconsistent_with_data}
% Remove this section?





\end{document}
