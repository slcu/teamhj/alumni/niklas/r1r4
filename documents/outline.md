# Modelling outline

1. Simple model -- How does cytokinin affect flux? Import vs Export?
  - Export yields slower dynamics.
  - We speculate that the export model makes more sense.
    - Inhibition of export is more common than promotion of import.
    - Export ensures slower dynamics in the precense of CYT. This makes it easier to match the relatively slower response to the CYT treatment than to the peak. 
      - In order for the import model to be sufficently slow during a CYT treatment (which speeds up dynamics), the general dynamics without CYT must be slower than we would expect from the real system.
  - We pick the export model for further analysis.

- Figure 1
  - Export vs import time trajectory plots.
  - Export vs import equilibration time plots.
  - ? Import model becomes too slow in general if we try to fit it to the treatment data? (We don't have this)

2. Simple model -- Can the model explain both CYT treatment and WT CYT pulse?
  - Yes, but not using the same parameter values.
    - Proof by looing at the pre-peak simulations? They are identical until the time at which the peak starts to decline. It is, therefore, impossible for the model do display two different behaviours during this simulated time. Thus, if we demand that the response is fast in one case and slow in another, this cannot be fulfilled using the same parameter sets in the two cases.
  - Nuclear shuttling needs to be faster in the pulse case than in the treatment case.
    - IMPA3/6 is expressed just before normal cell division. Could this be the source of differential trafficing rates?

- Figure 2
  - Fast vs slow nuclear shuttling.
    - One fits pulse, the other fits treatment.
  - 

3. Simple feedback model -- Can the nuclear MYB3R4 feedback explain the differential timing?
  - Feedback exists only just before cell division (true?).
    - IMPA3/6 is increased at the right time, but does that really imply a broken/whole feedback loop?
  - Yes.
    - From data: Importin levels are a few times higher. This is reflected in the model.
    - The important thing is the increased availiblity of Importin. The fact that this is in feedback with could possibly(?) help with the correct timing of importing production. (This would not be reflected well in our current simulation since the CYT imput occurrs at the same time after the start of the simulation).
    - Feedback itself might not be necessary, only increased importin levels. Feedback could, however, ensure that the increase of importin occurs at the correct time.
      - Feedback importin vs simply increased importin. 

Additional information

4. The feedback in itself (without a trigger) could provide a sharp R4 relocation but not one that is consistent with all data.
  - Timing becomes strange.
  - Not consistent with pUBQ10::MYB3R4
  - Not consistent with CHX (at least in the way we interpret the experiment)
